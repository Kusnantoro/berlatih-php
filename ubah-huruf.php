<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sanbercode-Ubah-Huruf</title>
</head>

<body>
    <h1>Ubah Huruf</h1>
    <?php 
        function ubah_huruf($kata) {
            $abjad = "abcdefghijklmnopqrstuvwxyz";
            $output = "";
            for ($i=0; $i < strlen($kata) ; $i++) { 
                $posisi = strpos($abjad, $kata[$i]);
                $output .= substr($abjad, $posisi + 1, 1);
            }
            return $output . "<br>";
        };

        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu
    ?>
</body>

</html>